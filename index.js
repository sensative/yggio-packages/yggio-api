'use strict';

const _ = require('lodash');
const request = require('request-promise');
const fs = require('fs');

// This takes care of yggio dependencies, and forces
// yggioConfig to be set before use, otherwise errors will be thrown

const defaultConfig = {
  yggioUrl: null,
  rulesUrl: null,
  accessTokenKey: 'accessToken',
  refreshTokenKey: 'refreshToken',
  expiresAtKey: 'expiresAt',
  onTokenRefresh: null
};
let config = _.assign({}, defaultConfig);
let yggioBaseRequest = null;
let rulesBaseRequest = null;
const setYggioConfig = (cfg) => {
  const keys = _.keys(defaultConfig);
  config = _.assign({}, defaultConfig, _.pick(cfg, keys));

  yggioBaseRequest = request.defaults({
    baseUrl: config.yggioUrl,
    json: true
  });
  rulesBaseRequest = request.defaults({
    baseUrl: config.rulesUrl,
    json: true
  });
};
// // This stuff is used through yggio-service provider
let providerDetails = null;
const getProviderDetails = () => _.assign({}, providerDetails);

const requestObject = (token, uri, params, data) => {
  return {
    auth: {
      bearer: token
    },
    qs: params,
    uri: uri,
    body: data
  };
};
//
//
// /// Setup related stuff
//
//
// Register an yggio user
const register = (user) => {
  // user is { "username": "myName", "password": "myPassword"}
  return yggioBaseRequest.post(requestObject(null, '/api/users', {}, user))
  // and make it look like any yggio-user
    .then(res => ({[config.accessTokenKey]: res.token}));
};
// Login to storm with the service account
const login = (user) => {
  // user is { "username": "myName", "password": "myPassword"}
  // NOTE: an yggio user is effectively returned, but until further notice
  // it is under the guise of {token: 'stuff'} instead of
  // {accessToken: 'asdf', refreshToken: 'asdd'}. So. We return an object
  // containing just the 'accessToken'
  return yggioBaseRequest.post(requestObject(null, '/auth/local', {}, user))
  // and make it look like any yggio-user
    .then(res => ({[config.accessTokenKey]: res.token}));
};

// yep.. registers a provider
const registerProvider = (token, provider) => {
  return yggioBaseRequest.post(requestObject(token, '/api/provider', {}, provider))
    .then(details => {
      providerDetails = details;
      return details;
    });
};

const oauthCode = (code, redirectUri) => {
  return request.post({
    url: config.yggioUrl + '/auth/oauth/token',
    headers: {
      'Authorization': 'Basic ' + Buffer.from(providerDetails.client_id + ':' + providerDetails.secret).toString('base64')
    },
    json: {
      code: code, // req.params.code,
      grant_type: 'authorization_code',
      redirect_uri: redirectUri
    }
  })
  .then(res => {
    const codeUser = {
      [config.accessTokenKey]: _.get(res, 'access_token'),
      [config.refreshTokenKey]: _.get(res, 'refresh_token'),
      [config.expiresAtKey]: _.get(res, 'expiresAt')
    };
    if (!codeUser[config.accessTokenKey]) {
      return Promise.reject(new Error('Yggio User Not Found.'));
    }
    return wrapRefresh(getMe(codeUser[config.accessTokenKey])
      .then(user => {
        const freshUser = _.assign({}, user, codeUser)
        delete freshUser._id;
        return freshUser;
      }));
  });
};

const setLogo = (token, logoPath) => {
  console.log('setLogo token: ', token);
  console.log('setLogo logoPath: ', logoPath);
  const providerId = providerDetails._id;
  if (!providerId) {
    return Promise.reject(new Error('provider not set'));
  }
  return yggioBaseRequest.post('api/providers/' + providerId + '/logo', {
    auth: {
      bearer: token
    },
    formData: {
      file: fs.createReadStream(logoPath)
    }
  });
};
// subscribes to a channel
const subscribe = (token, subscription) => {
  const fullSubscription = _.assign({}, subscription, {
    provider: providerDetails._id
  });
  // fullSubscription = {provider: providerId, iotnode: nodeId, type: subscriptionType}
  return yggioBaseRequest.post(requestObject(token, '/api/channels/', {}, fullSubscription))
};

//
//
// /// api related stuff
//
//

// commands a node to do something
const command = (token, data) => {
  console.log('doing the command: ', data);
  const uri = '/api/iotnodes/command';
  return yggioBaseRequest.put(requestObject(token, uri, {}, data));
};

// Just like totally gets me
const getMe = (token) => {
  const uri = '/api/users/me';
  return yggioBaseRequest.get(requestObject(token, uri));
};
const logoutFromYggio = (token, id) => {
  const path = '/api/users/' + id + '/logout';
  return yggioBaseRequest.post(requestObject(token, path));
};
const getNode = (token, id) => {
  const url = '/api/iotnodes/' + id;
  return yggioBaseRequest.get(requestObject(token, url));
};
const getNodes = (token) => {
  const url = '/api/iotnodes?limit=100000';
  return yggioBaseRequest.get(requestObject(token, url));
};
const getAccessibleNodes = (token) => {
  const url = '/api/iotnodes/shared?limit=100000';
  return yggioBaseRequest.get(requestObject(token, url));
};
const createNode = (token, node) => {
  const url = 'api/iotnodes';
  return yggioBaseRequest.post(requestObject(token, url, {}, node));
};
const updateNode = (token, nodeId, data) => {
  const url = '/api/iotnodes/' + nodeId;
  return yggioBaseRequest.put(requestObject(token, url, {}, data));
};
const deleteNode = (token, nodeId) => {
  const url = '/api/iotnodes/' + nodeId;
  return yggioBaseRequest.delete(requestObject(token, url));
};


// Oauth
//
const checkOAuthStatus = (token, nodeType) => {
  const url = '/api/oauth';
  return yggioBaseRequest.get(requestObject(token, url, {nodeType}));
};
const fetchOAuthClientNodes = (token, nodeType) => {
  const url = '/api/iotnodes/command';
  return yggioBaseRequest.put(requestObject(token, url, {nodeType, waitFor: 'success'}, {command: 'getSystems'}));
};

//
//
// /// rules
//
//
const getRulesStates = (token) => {
  const url = '/api/states';
  return rulesBaseRequest.get(requestObject(token, url));
};
const setRulesState = (token, stateId, data) => {
  const url = '/api/states/' + stateId;
  return rulesBaseRequest.put(requestObject(token, url, {}, data));
};
const getRule = (token, ruleId) => {
  const url = '/api/rules/' + ruleId;
  return rulesBaseRequest.get(requestObject(token, url));
};
const getRules = (token) => {
  const url = '/api/rules';
  return rulesBaseRequest.get(requestObject(token, url));
};
const executeRule = (token, ruleId) => {
  const url = '/api/rules/activate/' + ruleId;
  return rulesBaseRequest.put(requestObject(token, url));
};

//
//
// ARE THESE USED ???????
//
const getScenarioTemplates = (token) => {
  const url = '/api/templates';
  return rulesBaseRequest.get(requestObject(token, url));
};
const createScenario = (token, templateId) => {
  // Apply template does not remove existing rules
  const url = '/api/templates/' + templateId + '/apply';
  return rulesBaseRequest.put(requestObject(token, url));
};
//
// ARE THESE USED ???????
//
//
const getRulesConditions = (token) => {
  const url = '/api/conditions';
  return rulesBaseRequest.get(requestObject(token, url));
};

const updateRulesCondition = (token, condition, conditionId) => {
  const url = '/api/conditions/' + conditionId;
  return rulesBaseRequest.put(requestObject(token, url, {}, condition));
};

const createRulesCondition = (token, condition) => {
  const url = '/api/conditions';
  return rulesBaseRequest.post(requestObject(token, url, {}, condition));
};

const getRulesActions = (token) => {
  const url = '/api/actions';
  return rulesBaseRequest.get(requestObject(token, url));
};

const createRulesAction = (token, action) => {
  const url = '/api/actions';
  return rulesBaseRequest.post(requestObject(token, url, {}, action));
};

const updateRulesAction = (token, action) => {
  const url = '/api/actions/' + action._id;
  return rulesBaseRequest.put(requestObject(token, url, {}, action));
};

const deleteRulesAction = (token, actionId) => {
  const url = '/api/actions/' + actionId;
  return rulesBaseRequest.delete(requestObject(token, url));
};

const createRule = (token, rule) => {
  const url = 'api/rules';
  return rulesBaseRequest.post(requestObject(token, url, {}, rule));
};

const deleteRule = (token, ruleId) => {
  const url = 'api/rules/' + ruleId;
  return rulesBaseRequest.delete(requestObject(token, url));
};

const updateRule = (token, rule) => {
  const url = '/api/rules/' + rule._id;
  return rulesBaseRequest.put(requestObject(token, url, {}, rule));
};

const addRuleTrigger = (token, ruleId, trigger) => {
  const url = '/api/rules/' + ruleId + '/addtrigger';
  return rulesBaseRequest.put(requestObject(token, url, {}, trigger));
};

const addRuleEvent = (token, ruleId, event) => {
  const url = '/api/rules/' + ruleId + '/addevent';
  return rulesBaseRequest.put(requestObject(token, url, {}, event));
};

const getContacts = token => {
  const url = '/api/contacts';
  return rulesBaseRequest.get(requestObject(token, url));
};

const setContacts = (token, contacts) => {
  const url = '/api/contacts';
  return rulesBaseRequest.put(requestObject(token, url, {}, contacts));
};

//
//
// /// wrap up api calls in refresh token stuffs
//
//
const wrapRefresh = routes => {
  // this is the api call to refresh the token. is not externally exposed
  const refreshUser = (refreshToken) => {
    return request.post({
      url: config.yggioUrl + '/auth/oauth/token',
      headers: {
        'Authorization': 'Basic ' + Buffer.from(providerDetails.client_id + ':' + providerDetails.secret).toString('base64')
      },
      json: {
        grant_type: 'refresh_token',
        refresh_token: refreshToken
      }
    });
  };
  // a function that checks if the error is due to a needed token refresh
  const isInvalidUser = err => {
    // TODO - right now this is never refreshing (Max?)
    return false;
  };
  // the pointer to the copy object is actually returned,
  // that is why we do the copy in the first place
  const copy = _.assignWith({}, routes, (__r, route) => {
    // use function for access to args
    const wrappedRoute = function (...args) {
      if (!args.length) {
        return Promise.reject(new Error('no user'));
      }
      // do the user <-> tokens magic
      const user = arguments[0];
      const token = _.get(user, config.accessTokenKey, user); // default user: backwards compatibility storm_access_token is first argument instead of a user object
      const refreshToken = _.get(user, config.refreshTokenKey);
      // args need modification user <--> user.accessToken
      const modifiedArgs = [token].concat(args.slice(1))
      // and run
      return route.apply(null, modifiedArgs)
        .catch(err => {
          // if we have set the provider stuff, and callback, and if the error
          // is due to a needed token refresh, then do so if possible
          if (!isInvalidUser(err) || !providerDetails || !config.onTokenRefresh || !refreshToken) {
            return Promise.reject(err);
          }
          return refreshUser(refreshToken)
            .then(refreshedUser => {
              // locally store the new token for use below
              modifiedArgs[0] = _.get(refreshUser, config.accessTokenKey);
              // and let app save the new token
              return config.onTokenRefresh(user, refreshedUser);
            })
            .then(() => {
              // and re-execute the route
              return route.apply(null, modifiedArgs);
            });
        });
    }
    return wrappedRoute;
  });

  return copy;
};

const wrappedExports = {
  registerProvider,
  setLogo,
  subscribe,
  command,
  getMe,
  logoutFromYggio,
  getNode,
  getNodes,
  getAccessibleNodes,
  createNode,
  updateNode,
  deleteNode,
  checkOAuthStatus,
  fetchOAuthClientNodes,
  getRulesStates,
  setRulesState,
  getRule,
  getRules,
  executeRule,
  getScenarioTemplates,
  createScenario,
  getRulesConditions,
  updateRulesCondition,
  createRulesCondition,
  getRulesActions,
  createRulesAction,
  updateRulesAction,
  deleteRulesAction,
  createRule,
  deleteRule,
  updateRule,
  addRuleTrigger,
  addRuleEvent,
  getContacts,
  setContacts
};

const unwrappedExports = {
  setYggioConfig,
  register,
  login,
  getProviderDetails,
  oauthCode
};

module.exports = _.assign({}, wrapRefresh(wrappedExports), unwrappedExports);
