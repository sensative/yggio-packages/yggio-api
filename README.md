## yggio-api: the public parts

yggio-api exposes the public api of yggio as a module. sets up the boilerplate stuff and is meant to be both a guide and aid, but is not in and of itself necessary.

### Usage

First, yggio-api needs to be initialized with config info (to know where to find yggio mainly)
```javascript
const yggioApi = require('yggio-api');
// rules engine and the yggio rest-api have different urls that need to be supplied
const yggioConfig = {
  rulesUrl: 'my-rules-url.com:3315',
  yggioUrl: '192.169.2.3:2003'
};
yggioApi.setYggioConfig(yggioConfig);
```
Once, the service provider has registered (yggio-subscriber does this stuff), we have the providerDetails necessary for oauth to refresh yggio access tokens (`storm_access_token`)
```javascript
yggioApi.initializeRefresh({
  providerDetails, // {token}
  // this callback MUST return a promise. The return value is NOT used
  userWasRefreshed: (originalUser, refreshedUser) => {
    // example of what you probably want to do depending upon the schema of your user
    originalUser['storm_access_token'] = refreshedUser['storm_access_token'];
    return originalUser.save();
  };
});
```
With valid `providerDetails`, and as long as a `userWasRefreshed` callback function is supplied, the storm_access_token automatically gets refreshed when necessary.

The following routes are available:
```javascript
const routes = {
  // provider specific functionality
  registerProvider,
  setLogo,
  subscribe,
  // device
  setStatus,
  getStatus,
  awaitStatus,
  getMe,
  logoutFromYggio,
  getNode,
  getNodes,
  getAccessibleNodes,
  createNode,
  updateNode,
  checkOAuthStatus,
  fetchOAuthClientNodes,
  // rules
  getRulesStates,
  setRulesState,
  getRule,
  getRules,
  executeRule,
  getScenarioTemplates,
  createScenario,
  getRulesConditions,
  updateRulesCondition,
  createRulesCondition,
  getRulesActions,
  createRulesAction,
  updateRulesAction,
  deleteRulesAction,
  createRule,
  deleteRule,
  updateRule,
  addRuleTrigger,
  addRuleEvent,
  getContacts,
  setContacts
};
// these routes all require as first argument a user object with the required parameters
const user = {
  storm_access_token,
  storm_refresh_token
};
```
**Warning: expect breaking changes between minor versions**

It is likely that some of the functionality will be changed/removed/renamed

### License
yggio-api is under the MIT license. See LICENSE file.
